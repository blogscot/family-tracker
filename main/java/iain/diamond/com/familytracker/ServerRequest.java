package iain.diamond.com.familytracker;

import android.content.ContentValues;
import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class ServerRequest {

  public static final int CONNECTION_TIMEOUT = 1000 * 15;
  public static final String SERVER_ADDRESS = "http://46.101.92.161/login/";
  private final String USER_AGENT = "Mozilla/5.0";
  private Context context;

  public ServerRequest(Context context) {
    this.context = context;
  }

  /**
   * Store user location information as a background task
   *
   * @param user        username
   * @param location    latitude, longitude
   * @param callback    callback function
   */

  public void storeLocationDataInBackground(User user, Location location, CallbackMethod callback) {
    if (isConnected()) {
      new StoreLocationDataAsyncTask(user, location, callback).execute();
    }
  }

  /**
   * Post User GPS co-ordinates to server as AsyncTask
   */

  public class StoreLocationDataAsyncTask extends AsyncTask<Void, Void, Void> {
    User user;
    Location location;
    CallbackMethod callback;

    public StoreLocationDataAsyncTask(User user, Location location, CallbackMethod callback) {
      this.user = user;
      this.location = location;
      this.callback = callback;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      callback.done(null);
      super.onPostExecute(aVoid);
    }

    @Override
    protected Void doInBackground(Void... params) {

      try {
        storeLocationData();
      } catch (IOException e) {
        e.printStackTrace();
      }
      return null;
    }

    /**
     * Store user location information to the remote server
     *
     * @throws IOException
     */

    private void storeLocationData() throws IOException {

      Log.d("Family Tracker, POST", "Is Connected: " + isConnected());

      URL url = new URL(SERVER_ADDRESS + "location.php");
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();

      ContentValues dataToSend = new ContentValues();
      dataToSend.put(GPSInfo.USERNAME, user.username);
      dataToSend.put(GPSInfo.LATITUDE, location.getLatitude());
      dataToSend.put(GPSInfo.LONGITUDE, location.getLongitude());

      //add request header
      conn.setRequestMethod("POST");
      conn.setRequestProperty("User-Agent", USER_AGENT);
      conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
      conn.setConnectTimeout(CONNECTION_TIMEOUT);
      conn.setReadTimeout(CONNECTION_TIMEOUT);

      String urlParameters = encodePostDataString(dataToSend);

      // Send post request
      conn.setDoOutput(true);
      DataOutputStream writer = new DataOutputStream(conn.getOutputStream());
      writer.writeBytes(urlParameters);
      writer.flush();
      writer.close();

      // Get the http response
      BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      String inputLine;
      StringBuffer response = new StringBuffer();

      while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
      }
      in.close();
    }
  }


  /**
   * Store user Phone information as a background task
   *
   * @param user        username
   * @param outgoing    the outgoing number
   * @param incoming    the incoming number
   * @param callback    callback function
   */

  public void storePhoneDataInBackground(User user,
                                         String outgoing,
                                         String incoming,
                                         CallbackMethod callback) {
    if (isConnected()) {
      new StorePhoneDataAsyncTask(user, outgoing, incoming, callback).execute();
    }
  }

  /**
   * Post User GPS co-ordinates to server as AsyncTask
   */

  public class StorePhoneDataAsyncTask extends AsyncTask<Void, Void, Void> {
    User user;
    String outgoing;
    String incoming;
    CallbackMethod callback;

    public StorePhoneDataAsyncTask(User user, String outgoing, String incoming, CallbackMethod callback) {
      this.user = user;
      this.outgoing = outgoing;
      this.incoming = incoming;
      this.callback = callback;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      callback.done(null);
      super.onPostExecute(aVoid);
    }

    @Override
    protected Void doInBackground(Void... params) {

      try {
        storePhoneData();
      } catch (IOException e) {
        e.printStackTrace();
      }
      return null;
    }

    /**
     * Store user location information to the remote server
     *
     * @throws IOException
     */

    private void storePhoneData() throws IOException {

      Log.d("Family Tracker, POST", "Is Connected: " + isConnected());

      URL url = new URL(SERVER_ADDRESS + "phone.php");
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();

      ContentValues dataToSend = new ContentValues();
      dataToSend.put(PhoneInfo.USERNAME, user.username);
      dataToSend.put(PhoneInfo.OUTGOING, outgoing);
      dataToSend.put(PhoneInfo.INCOMING, incoming);

      //add request header
      conn.setRequestMethod("POST");
      conn.setRequestProperty("User-Agent", USER_AGENT);
      conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
      conn.setConnectTimeout(CONNECTION_TIMEOUT);
      conn.setReadTimeout(CONNECTION_TIMEOUT);

      String urlParameters = encodePostDataString(dataToSend);

      // Send post request
      conn.setDoOutput(true);
      DataOutputStream writer = new DataOutputStream(conn.getOutputStream());
      writer.writeBytes(urlParameters);
      writer.flush();
      writer.close();

      // Get the http response
      BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      String inputLine;
      StringBuffer response = new StringBuffer();

      while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
      }
      in.close();
    }
  }

  /**
   * Request user location information from the remote server
   *
   * @param callback      Callback function
   */

  public void getLocationDataInBackground(CallbackMethod callback) {
    new getLocationDataAsyncTask(callback).execute();
  }

  /**
   * Get User GPS co-ordinates from server as AsyncTask
   */

  public class getLocationDataAsyncTask extends AsyncTask<Void, Void, ArrayList> {
    CallbackMethod callback;

    public getLocationDataAsyncTask(CallbackMethod callback) {
      this.callback = callback;
    }

    @Override
    protected void onPostExecute(ArrayList list) {
      callback.done(list);
      super.onPostExecute(list);
    }

    @Override
    protected ArrayList<ContentValues> doInBackground(Void... params) {

      if (!isConnected()) {
        return new ArrayList<>();
      }

      try {
        return getLocationData();
      } catch (IOException e) {
        Log.e("Family Tracker: ", e.getMessage());
      }
      // return empty values
      return new ArrayList<>();
    }

    private ArrayList<ContentValues> getLocationData() throws IOException {

      Log.d("Family Tracker, GET ", "Is Connected: " + isConnected());

      URL url = new URL(SERVER_ADDRESS + "getlocations.php");
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();

      //add request header
      conn.setRequestMethod("GET");
      conn.setRequestProperty("User-Agent", USER_AGENT);
      conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
      conn.setConnectTimeout(CONNECTION_TIMEOUT);
      conn.setReadTimeout(CONNECTION_TIMEOUT);

      // Send get request
      int responseCode = conn.getResponseCode();

      // Get the http response
      BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      String inputLine;
      StringBuffer response = new StringBuffer();

      while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
      }
      in.close();

      // process the response
      ArrayList<ContentValues> returnedList = new ArrayList<>();

      JSONArray jsonArray = null;
      try {
        jsonArray = new JSONArray(String.valueOf(response));
        if (jsonArray.length() != 0) {

          // holds data for each register user
          for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            ContentValues returnedValues = new ContentValues();

            String username = jsonObject.getString(GPSInfo.USERNAME);
            String latitude = jsonObject.getString(GPSInfo.LATITUDE);
            String longitude = jsonObject.getString(GPSInfo.LONGITUDE);
            String timestamp = jsonObject.getString(GPSInfo.TIMESTAMP);

            returnedValues.put(GPSInfo.USERNAME, username);
            returnedValues.put(GPSInfo.LATITUDE, latitude);
            returnedValues.put(GPSInfo.LONGITUDE, longitude);
            returnedValues.put(GPSInfo.TIMESTAMP, timestamp);

            storeUserDetailsInLocalDatabase(username, latitude, longitude, timestamp);

            returnedList.add(returnedValues);
          }
        }
      } catch (JSONException e) {
        Log.d("ServerRequest:", "Unable to convert '" + String.valueOf(response) + "' to JSON Object");
      }
      return returnedList;
    }
  }

  /**
   * Request user location information from the remote server
   *
   * @param callback      Callback function
   */

  public void getPhoneDataInBackground(CallbackMethod callback) {
    new getPhoneDataAsyncTask(callback).execute();
  }

  /**
   * Get User GPS co-ordinates from server as AsyncTask
   */

  public class getPhoneDataAsyncTask extends AsyncTask<Void, Void, ArrayList> {
    CallbackMethod callback;

    public getPhoneDataAsyncTask(CallbackMethod callback) {
      this.callback = callback;
    }

    @Override
    protected void onPostExecute(ArrayList list) {
      callback.done(list);
      super.onPostExecute(list);
    }

    @Override
    protected ArrayList<ContentValues> doInBackground(Void... params) {

      if (!isConnected()) {
        return new ArrayList<>();
      }

      try {
        return getPhoneData();
      } catch (IOException e) {
        Log.e("Family Tracker: ", e.getMessage());
      }
      // return empty values
      return new ArrayList<>();
    }

    private ArrayList<ContentValues> getPhoneData() throws IOException {

      Log.d("Family Tracker, GET ", "Is Connected: " + isConnected());

      URL url = new URL(SERVER_ADDRESS + "getPhoneRecords.php");
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();

      //add request header
      conn.setRequestMethod("GET");
      conn.setRequestProperty("User-Agent", USER_AGENT);
      conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
      conn.setConnectTimeout(CONNECTION_TIMEOUT);
      conn.setReadTimeout(CONNECTION_TIMEOUT);

      // Send get request
      int responseCode = conn.getResponseCode();

      // Get the http response
      BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      String inputLine;
      StringBuffer response = new StringBuffer();

      while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
      }
      in.close();

      // process the response
      ArrayList<ContentValues> returnedList = new ArrayList<>();

      JSONArray jsonArray = null;
      try {
        jsonArray = new JSONArray(String.valueOf(response));
        if (jsonArray.length() != 0) {

          // holds data for each register user
          for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            ContentValues returnedValues = new ContentValues();

            String username = jsonObject.getString(PhoneInfo.USERNAME);
            String latitude = jsonObject.getString(PhoneInfo.OUTGOING);
            String longitude = jsonObject.getString(PhoneInfo.INCOMING);
            String timestamp = jsonObject.getString(PhoneInfo.TIMESTAMP);

            returnedValues.put(PhoneInfo.USERNAME, username);
            returnedValues.put(PhoneInfo.OUTGOING, latitude);
            returnedValues.put(PhoneInfo.INCOMING, longitude);
            returnedValues.put(PhoneInfo.TIMESTAMP, timestamp);

            returnedList.add(returnedValues);
          }
        }
      } catch (JSONException e) {
        Log.d("ServerRequest:", "Unable to convert '" + String.valueOf(response) + "' to JSON Object");
      }
      return returnedList;
    }
  }


  //
  // Private functions
  //

  private long storeUserDetailsInLocalDatabase(String username, String latitude, String longitude,
                                              String timestamp) {
    DBManager db = new DBManager(context);

    db.open();
    long result = db.insertData(username, latitude, longitude, timestamp);
    db.close();

    return result;
  }

  /**
   * Check for network connectivity (i.e. Wifi or Telephony)
   *
   * @return            True if network is found
   */

  private boolean isConnected() {
    ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

    NetworkInfo info = manager.getActiveNetworkInfo();
    return info != null && info.isConnectedOrConnecting();
  }

  /**
   * Encode values as URL encoded string
   *
   * @param values      values to encode
   * @return            True is encoding was successful
   * @throws UnsupportedEncodingException
   */

  private String encodePostDataString(ContentValues values) throws UnsupportedEncodingException {
    StringBuilder result = new StringBuilder();
    boolean first = true;

    for (String key : values.keySet()) {
      if (first) {
        first = false;
      } else {
        result.append("&");
      }
      result.append(URLEncoder.encode(key, "UTF-8"));
      result.append("=");
      result.append(URLEncoder.encode(values.getAsString(key), "UTF-8"));
    }
    return result.toString();
  }
}
