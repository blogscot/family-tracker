package iain.diamond.com.familytracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.util.ArrayList;

public class IncomingCallInterceptor extends BroadcastReceiver {

  private static final String TAG = "IncomingCall:";

  @Override
  public void onReceive(Context context, Intent intent) {
    String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

    if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
      String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
      ServerRequest request = new ServerRequest(MainActivity.getInstance());

      User testUser = MainActivity.testUser;

      request.storePhoneDataInBackground(testUser, "", incomingNumber, new CallbackMethod() {
        @Override
        public void done(ArrayList arrayList) {
          Log.i(TAG, "Incoming call logged ");
        }
      });
    }
  }
}
