package iain.diamond.com.familytracker;

import android.content.ContentValues;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback {

  private GoogleMap mMap;
  private LatLng marker = null;

  String username, timestamp;
  // default lat, long
  Double latitude = 56.465;
  Double longitude = -3.0468;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_maps);
    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
            .findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);

    // read initial user information
    ArrayList<ContentValues> values = getIntent().getParcelableArrayListExtra("location_data");

    if (values.size() > 0) {
      // Process info for first user
      ContentValues value = values.get(0);
      username = value.getAsString(GPSInfo.USERNAME);
      latitude = value.getAsDouble(GPSInfo.LATITUDE);
      longitude = value.getAsDouble(GPSInfo.LONGITUDE);
      timestamp = value.getAsString(GPSInfo.TIMESTAMP);
    }

    marker = new LatLng(latitude, longitude);
    pollLocationData();
  }

  /**
   *  Poll server for the latest user GPS information
   *
   */

  private void pollLocationData() {
    Timer polling_timer = new Timer();

    polling_timer.schedule(new TimerTask() {

      @Override
      public void run() {

        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            ServerRequest serverRequest = new ServerRequest(MapsActivity.this);
            serverRequest.getLocationDataInBackground(new CallbackMethod() {
              @Override
              public void done(ArrayList arrayList) {

                // Check server returned good info
                if (arrayList.size() != 0) {
                  Log.i("Family Tracker: ", "Poll Info received.");

                  mMap.clear();
                  // show user markers
                  for (int i = 0; i < arrayList.size(); i++) {
                    ContentValues userValues = (ContentValues) arrayList.get(i);
                    showUser(userValues);
                  }

                } else {
                  // Display the users info from the local database
                  DBManager db = new DBManager(MapsActivity.this);
                  mMap.clear();

                  db.open();
                  ArrayList<String> users = db.getUsernames();
                  for (String user: users) {
                    ContentValues userInfo = db.getUserInfo(user);
                    showUser(userInfo);
                  }
                  db.close();
                }
              }
            });
          }
        });
      }
    }, 0, 30000);
  }

  private void showUser(ContentValues values) {
    username = values.getAsString(GPSInfo.USERNAME);
    Double latitude = values.getAsDouble(GPSInfo.LATITUDE);
    Double longitude = values.getAsDouble(GPSInfo.LONGITUDE);
    timestamp = values.getAsString(GPSInfo.TIMESTAMP);

    marker = new LatLng(latitude, longitude);
    addMarker(username, "Timestamp: " + timestamp, marker);
  }

  /**
   * Manipulates the map once available.
   * This callback is triggered when the map is ready to be used.
   * This is where we can add markers or lines, add listeners or move the camera. In this case,
   * we just add a marker near Sydney, Australia.
   * If Google Play services is not installed on the device, the user will be prompted to install
   * it inside the SupportMapFragment. This method will only be triggered once the user has
   * installed Google Play services and returned to the app.
   */
  @Override
  public void onMapReady(GoogleMap map) {
    mMap = map;
    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker, 15));
    addMarker(username, "Timestamp: " + timestamp, marker);
  }

  /**
   * Convenience function for adding map markers
   * @param title       marker title
   * @param snippet     marker snippet text
   * @param marker      the marker object
   */

  public void addMarker(String title, String snippet, LatLng marker) {
    BitmapDescriptor descriptor = isConnected() ?
            BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED) :
            BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE);

    mMap.addMarker(new MarkerOptions()
            .title(title)
            .icon(descriptor)
            .snippet(snippet)
            .position(marker));
  }

  /**
   * Checks for Network availability
   *
   * @return true     network connectivity available
   */

  public boolean isConnected() {
    ConnectivityManager manager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

    NetworkInfo info = manager.getActiveNetworkInfo();
    return info != null && info.isConnectedOrConnecting();
  }

}

