package iain.diamond.com.familytracker;

import android.content.ContentValues;
import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

public class PhoneRecordsAdapter extends ArrayAdapter<ContentValues> {

  ArrayList<String> phoneRecords = new ArrayList<>();

  public PhoneRecordsAdapter(Context context, int resource, List<ContentValues> records) {
    super(context, resource, records);

    for (int i = 0; i < records.size(); i++) {
      ContentValues phoneRecord = records.get(i);
      String username = phoneRecord.getAsString(PhoneInfo.USERNAME);
      String outgoing = phoneRecord.getAsString(PhoneInfo.OUTGOING);
      String incoming = phoneRecord.getAsString(PhoneInfo.INCOMING);
      String timestamp = phoneRecord.getAsString(PhoneInfo.TIMESTAMP);

      phoneRecords.add(timestamp + ": " + username + ": " + outgoing + ": " + incoming);
    }
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public boolean hasStableIds() {
    return true;
  }
}
