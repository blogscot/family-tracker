package iain.diamond.com.familytracker;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, View.OnClickListener {

  private static final String TAG = "Family Tracker: ";

  private static final int UPDATE_INTERVAL = 20000;
  private static final int FASTEST_INTERVAL = 60000;
  private static final int MY_MULTIPLE_PERMISSIONS = 11;

  private static MainActivity that;

  Button getDataBtn, getPhoneRecordsBtn;

  private GoogleApiClient mGoogleApiClient = null;
  private LocationRequest locationRequest;
  public static User testUser = new User("iain", "password");

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    that = this;

    getDataBtn = (Button) findViewById(R.id.getDataBtn);
    getPhoneRecordsBtn = (Button) findViewById(R.id.getPhoneRecordsBtn);

    getDataBtn.setOnClickListener(this);
    getPhoneRecordsBtn.setOnClickListener(this);

    // Create an instance of GoogleAPIClient.
    if (mGoogleApiClient == null) {
      mGoogleApiClient = new GoogleApiClient.Builder(this)
              .addConnectionCallbacks(this)
              .addOnConnectionFailedListener(this)
              .addApi(LocationServices.API)
              .build();
    }
  }

  protected void onStart() {
    mGoogleApiClient.connect();  // let's start the ball rolling
    super.onStart();
  }

  @Override
  protected void onDestroy() {
    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    mGoogleApiClient.disconnect();
    super.onDestroy();
  }

  @Override
  public void onLocationChanged(Location location) {
    ServerRequest serverRequest = new ServerRequest(this);


    serverRequest.storeLocationDataInBackground(testUser, location, new CallbackMethod() {
      @Override
      public void done(ArrayList empty) {
        Log.i(TAG, "Server Request sent..");
      }
    });
  }

  /**
   *  Set up high priority location requests
   *
   */

  @Override
  public void onConnected(Bundle bundle) {

    // Get all the permissions
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this,
              new String[]{
                      Manifest.permission.ACCESS_FINE_LOCATION,
                      Manifest.permission.PROCESS_OUTGOING_CALLS
      }, MY_MULTIPLE_PERMISSIONS);
    } else {
      locationRequest = createLocationRequest();
    }
  }

  protected LocationRequest createLocationRequest() {
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
    }
    locationRequest = new LocationRequest();
    locationRequest.setInterval(UPDATE_INTERVAL);
    locationRequest.setFastestInterval(FASTEST_INTERVAL);
    locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    return locationRequest;
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    switch (requestCode) {
      case MY_MULTIPLE_PERMISSIONS:
        locationRequest = createLocationRequest();
        break;
    }
  }

  @Override
  public void onConnectionSuspended(int i) { }

  @Override
  public void onConnectionFailed(ConnectionResult connectionResult) { }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.getDataBtn:

        if (isConnected()) {
          ServerRequest serverRequest = new ServerRequest(this);

          // Get latest user location info from server
          serverRequest.getLocationDataInBackground(new CallbackMethod() {
            @Override
            public void done(ArrayList list) {
              // display user info on map
              Intent intent = new Intent(MainActivity.this, MapsActivity.class);
              intent.putParcelableArrayListExtra("location_data", list);
              startActivity(intent);
            }
          });
        } else {
          showErrorMessage("No network connectivity. Displaying the last saved results instead.");
        }
        break;

      case R.id.getPhoneRecordsBtn:
        startActivity(new Intent(this, PhoneRecordsActivity.class));
        break;
    }
  }

  /**
   * Checks for Network availability
   *
   * @return true     network connectivity available
   */

  public boolean isConnected() {
    ConnectivityManager manager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

    NetworkInfo info = manager.getActiveNetworkInfo();
    return info != null && info.isConnectedOrConnecting();
  }


  /**
   * Default Error message dialog
   *
   * @param msg     message to be shown to the user
   */

  private void showErrorMessage(String msg) {
    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
    dialogBuilder
            .setTitle("Warning")
            .setMessage(msg)
            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        // start Maps activity with null data
        Intent intent = new Intent(MainActivity.this, MapsActivity.class);
        intent.putParcelableArrayListExtra("location_data", new ArrayList<Parcelable>());
        startActivity(intent);
      }
    });
    dialogBuilder.show();
  }

  public static MainActivity getInstance() {
    return that;
  }


}
