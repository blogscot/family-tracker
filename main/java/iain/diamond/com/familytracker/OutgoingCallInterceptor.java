package iain.diamond.com.familytracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;

public class OutgoingCallInterceptor extends BroadcastReceiver {

  private static final String TAG = "OutgoingCall:";

  @Override
  public void onReceive(Context context, Intent intent) {
    String outgoingNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

    User testUser = MainActivity.testUser;

    ServerRequest request = new ServerRequest(MainActivity.getInstance());
    request.storePhoneDataInBackground(testUser, outgoingNumber, "", new CallbackMethod() {
      @Override
      public void done(ArrayList arrayList) {
        Log.i(TAG, "Outgoing call logged ");
      }
    });
  }
}
