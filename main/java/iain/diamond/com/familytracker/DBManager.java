package iain.diamond.com.familytracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DBManager {

  private static final String TAG = DBManager.class.getSimpleName();

  private static final String DB_NAME = "sqlite_example.db";
  private static final int DB_VERSION = 1;
  private static final String TABLE = "locations";

  private Context context;
  private DatabaseHelper myDBHelper;
  private SQLiteDatabase db;

  private static final String[] columns = {
          GPSInfo.ID,
          GPSInfo.USERNAME,
          GPSInfo.LATITUDE,
          GPSInfo.LONGITUDE,
          GPSInfo.TIMESTAMP
  };

  public DBManager(Context context) {
    this.context = context;
    myDBHelper = new DatabaseHelper(context);
  }

  public DBManager open() {
    db = myDBHelper.getWritableDatabase();
    return this;
  }

  public void close() {
    myDBHelper.close();
  }

  /**
   * Stores user GPS information in SQLite database. One entry is stored for each user
   *
   * @param username      the user's username
   * @param latitude      the user's latitude
   * @param longitude     the user's longitude
   * @param timestamp     the time the information was logged on the remote server
   * @return              -1 if an error occurred
   */
  public long insertData(String username, String latitude, String longitude, String timestamp) {

    if (checkUserExists(username)) {
      return updateData(username, latitude, longitude, timestamp);
    } else {
      ContentValues values = new ContentValues();
      values.put(GPSInfo.USERNAME, username);
      values.put(GPSInfo.LATITUDE, latitude);
      values.put(GPSInfo.LONGITUDE, longitude);
      values.put(GPSInfo.TIMESTAMP, timestamp);

      return db.insert(TABLE, null, values);
    }
  }

  /**
   * Update a user's location information in the SQLite database
   *
   * @param username      the user's username
   * @param latitude      the user's latitude
   * @param longitude     the user's longitude
   * @param timestamp     the time the information was logged on the remote server
   * @return              the number of rows affected
   */
  public int updateData(String username, String latitude, String longitude, String timestamp) {
    String whereClause = GPSInfo.USERNAME + " =?";
    String[] whereArgs = {username};

    ContentValues values = new ContentValues();
    values.put(GPSInfo.USERNAME, username);
    values.put(GPSInfo.LATITUDE, latitude);
    values.put(GPSInfo.LONGITUDE, longitude);
    values.put(GPSInfo.TIMESTAMP, timestamp);

    return db.update(TABLE, values, whereClause, whereArgs);
  }

  /**
   * Check if the user exists in the SQLite database
   *
   * @param username      the user's username
   * @return              True if found
   */
  public boolean checkUserExists(String username) {
    return getUserInfo(username).size() > 0;
  }

  public void deleteAll() {

    try {
      db.execSQL("drop table if exists " + TABLE);
    } catch (Exception e) {
      Log.e(TAG, "deleteAll: ", e);
    }

    Log.i(TAG, "deleteAll: ");
  }

  /**
   * Deletes a user's information from the SQLite database
   *
   * @param username      the user's username
   * @return              the number of rows affected
   */
  public int deleteData(String username) {
    String whereClause = GPSInfo.USERNAME + " =?";
    String[] whereArgs = {username};

    return db.delete(TABLE, whereClause, whereArgs);
  }

  /**
   * Gets everything in the SQLite database
   *
   * @return              a string of all the entries
   */
  public String getAllData() {
    SQLiteDatabase db = myDBHelper.getReadableDatabase();
    String orderBy = GPSInfo.TIMESTAMP + " DESC";

    Cursor cursor = db.query(TABLE, columns, null, null, null, null, orderBy);
    StringBuffer buffer = new StringBuffer();

    while (cursor.moveToNext()) {
      int index = cursor.getColumnIndex(GPSInfo.ID);
      int id = cursor.getInt(index++);
      String username = cursor.getString(index++);
      String latitude = cursor.getString(index++);
      String longitude = cursor.getString(index++);
      String timestamp = cursor.getString(index++);
      buffer.append(id + " " + username + " " + latitude + " " + longitude + " " + timestamp + "\n");
    }
    cursor.close();
    return buffer.toString();
  }

  /**
   * Get all the usernames currently stored in the database
   *
   * @return ArrayList      usernames
   */
  public ArrayList<String> getUsernames() {
    SQLiteDatabase db = myDBHelper.getReadableDatabase();
    String orderBy = GPSInfo.TIMESTAMP + " DESC";

    String[] args = {
            GPSInfo.ID,
            GPSInfo.USERNAME,
    };

    Cursor cursor = db.query(TABLE, args, null, null, null, null, orderBy);
    ArrayList<String> usernames = new ArrayList<>();

    while (cursor.moveToNext()) {
      int index = cursor.getColumnIndex(GPSInfo.ID);
      int id = cursor.getInt(index++);
      String username = cursor.getString(index++);
      usernames.add(username);
    }
    cursor.close();
    return usernames;
  }

  /**
   * Returns a single user entry from the SQLite database
   * Only one entry is stored per user, which is updated
   * as new details are received.
   *
   * @param user      the user's username
   * @return          contains the users details: username, lat, long, and timestamp
   */
  public ContentValues getUserInfo(String user) {
    SQLiteDatabase db = myDBHelper.getReadableDatabase();
    String limit = "1";
    String orderBy = GPSInfo.TIMESTAMP + " DESC";

    String[] selectionArgs = {user};
    String selection = GPSInfo.USERNAME + "=?";

    Cursor c = db.query(TABLE, columns, selection, selectionArgs, null, null, orderBy, limit);
    ContentValues userInfo = new ContentValues();

    while (c.moveToNext()) {
      int index1 = c.getColumnIndex(GPSInfo.USERNAME);
      int index2 = c.getColumnIndex(GPSInfo.LATITUDE);
      int index3 = c.getColumnIndex(GPSInfo.LONGITUDE);
      int index4 = c.getColumnIndex(GPSInfo.TIMESTAMP);

      userInfo.put(GPSInfo.USERNAME, c.getString(index1));
      userInfo.put(GPSInfo.LATITUDE, c.getString(index2));
      userInfo.put(GPSInfo.LONGITUDE, c.getString(index3));
      userInfo.put(GPSInfo.TIMESTAMP, c.getString(index4));
    }

    c.close();
    return userInfo;
  }

  private static class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context) {
      super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
      String sql = String.format("create table %s (%s integer primary key autoincrement," +
                      " %s text, %s text, %s text, %s text)",
              TABLE,
              GPSInfo.ID,
              GPSInfo.USERNAME,
              GPSInfo.LATITUDE,
              GPSInfo.LONGITUDE,
              GPSInfo.TIMESTAMP
      );

      db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      db.execSQL("drop table if exists " + TABLE);
      onCreate(db);
    }
  }
}
