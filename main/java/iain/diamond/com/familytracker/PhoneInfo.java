package iain.diamond.com.familytracker;

public class PhoneInfo {
  public static final String USERNAME = "username";
  public static final String INCOMING = "incoming";
  public static final String OUTGOING = "outgoing";
  public static final String TIMESTAMP = "timestamp";
}

