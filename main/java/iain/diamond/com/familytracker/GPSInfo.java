package iain.diamond.com.familytracker;

import android.provider.BaseColumns;

public class GPSInfo {
  public static final String ID = BaseColumns._ID;
  public static final String USERNAME = "username";
  public static final String LATITUDE = "latitude";
  public static final String LONGITUDE = "longitude";
  public static final String TIMESTAMP = "timestamp";
}
