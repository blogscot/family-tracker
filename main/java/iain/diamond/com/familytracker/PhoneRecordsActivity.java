package iain.diamond.com.familytracker;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

public class PhoneRecordsActivity extends AppCompatActivity {

  private static final String TAG = "Family Tracker: ";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_phone_records);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ListView listView = (ListView) findViewById(R.id.listView);

    ServerRequest request = new ServerRequest(this);
    request.getPhoneDataInBackground(new CallbackMethod() {
      @Override
      public void done(ArrayList arrayList) {

        Log.i(TAG, "Phone details received");

        final PhoneRecordsAdapter adapter =
                new PhoneRecordsAdapter(PhoneRecordsActivity.this,
                        android.R.layout.simple_list_item_1, arrayList);
        listView.setAdapter(adapter);
      }
    });

  }



}
