package iain.diamond.com.familytracker;

import java.util.ArrayList;

public interface CallbackMethod {

  public abstract void done(ArrayList arrayList);
}
